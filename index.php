<?php include ('header.php');?>


<body data-spy="scroll" data-target=".navbar-scrolled" data-offset="0" class="scrollspy-example">

<aside class="nitesh-aside">

    <!-- control bar -->
    <div class="control-bar">
        <div class="text-center">
            <span class="menu-toggle">
                <i class="chart"></i>
                <i class="chart"></i>
                <i class="chart"></i>
            </span>
        </div>
        <p class="copyright">
            <a href="http://niteshsaini.com">Er.Nitesh Saini</a> - &copy; - made by love <i class="fa fa-heart"></i> <span id="test1"></span> <span id="test2"></span>
        </p>
    </div>

    <!-- Start SlideMenu
    ===================================-->
    <div class="slide-menu-container">
        <!-- start the right menu -->
        <div class="slide-menu menu-opend navbar-scrolled nitesh-menu-style-2">
            <div class="nitesh-user-info">
                <!-- Start Social media -->
<ul class="social-media list-unstyled text-center">
    <li><a href="https://www.facebook.com/Niteshsaini500" class="facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
    <li><a href="https://twitter.com/Niteshsaini100" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
    <li><a href="https://medium.com/@Niteshsaini500" class="medium" target="_blank"><i class="fa fa-medium"></i></a></li>
    <li><a href="https://www.linkedin.com/in/niteshsaini500/" class="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
    <li><a href="https://github.com/niteshsaini500" class="github" target="_blank"><i class="fa fa-github"></i></a></li>
</ul>
                <!-- End Social media -->
                <div class="nitesh-img-box">
                    <div class="nitesh-img">
                        <img src="images/11.jpg" alt=""/>
                    </div>
                </div>
            </div>
            <div class="nav">
                <!-- Start Menu Links -->
                <ul class="list-unstyled nitesh-links">
                    <li class="active">
                        <a href="#home"><i class="fa fa-home"></i>Home</a>
                    </li>
                    <li>
                        <a href="#aboutMe"><i class="glyphicon glyphicon-user"></i>About Me</a>
                    </li>
                    <li>
                        <a href="#portfolio"><i class="fa fa-laptop"></i>Portfolio</a>
                    </li>
                    <li>
                        <a href="#resume"><i class="glyphicon glyphicon-leaf"></i>Resume</a>
                    </li>
                    <li>
                        <a href="#blog"><i class="fa fa-newspaper-o"></i>Blog</a>
                    </li>
                    <li>
                        <a href="#contact"><i class="glyphicon glyphicon-headphones"></i>Contact Me</a>
                    </li>
                </ul>
                <!-- End Menu Links -->
            </div>
        </div>
        <!-- start the right menu -->
    </div>
    <!-- End SlideMenu ================-->

</aside>

<main class="main-page-content">

    <!-- Start Home
    ===================================-->
    <section class="full-page" id="home">
        <div class="home overlay-container">
            <div class="overlay">
                <div class="intro-section nitesh-container display-table">
                    <div class="display-table-cell">
                        <h3 class="nitesh-hello">HI, I AM</h3>
                        <h1 class="mr-nitesh">Nitesh Saini</h1>
                        <h3 class="nitesh-work-description">&nbsp;<span class="nitesh-work"></span></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Home ==================-->

    <!-- Start About Me
   ===================================-->
    <section class="full-page" id="aboutMe">
        <div class="aboutMe overlay-container">
            <!-- Intro Section -->
            <div class="nitesh-intro-section">
                <div class="overlay">
                    <div class="section-intro">
                            <div class="text-center nitesh-icon-box-container">
                                <div class="nitesh-icon-box">
                                    <span class="glyphicon glyphicon-user nitesh-icon"></span>
                                </div>
                            </div>
                            <h1 class="text-center">
                                ABOUT ME
                            </h1>
                        </div>
                </div>
            </div>
            <!-- Start know Me -->
            <div class="my-information">
                <div class="nitesh-container">
                    <div class="mrnitesh-main-title">
                        <h2>
                            KNOW ME
                            <span class="nitesh-line"></span>
                        </h2>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="my-image">
                                <img src="images/11.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="nitesh-info-detail">
                                <h3>HELLO,</h3>
                                <p>
                                    I’m Nitesh Saini. I extend myself here so you can get to know me on a personal level. My career as a Software Web Developer, my portfolio, my projects, my blog, my favorite quotes, and so much more.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start My Service -->
            <div class="nitesh-service">
                <div class="overlay">
                    <div class="nitesh-container">
                        <div class="mrnitesh-main-title">
                            <h2>
                                Service
                                <span class="nitesh-line"></span>
                            </h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-sm-6">
                                <div class="nitesh-icon-list-box">
                                    <div class="nitesh-icon-box-container">
                                        <div class="nitesh-icon-box">
                                            <span class="pe-7s-monitor nitesh-icon"></span>
                                        </div>
                                    </div>
                                    <div class="nitesh-list-info">
                                        <h4>Developement</h4>
                                        <p>
                                            I develop the web projects in various technology like Laravel, CodeIniter and Angular.Js.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="nitesh-icon-list-box">
                                    <div class="nitesh-icon-box-container">
                                        <div class="nitesh-icon-box">
                                            <span class="pe-7s-tools nitesh-icon"></span>
                                        </div>
                                    </div>
                                    <div class="nitesh-list-info">
                                        <h4>Integration</h4>
                                        <p>
                                            I can integret the API's like flight API, Hotel API, Social Login API, or any third party API's.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="nitesh-icon-list-box">
                                    <div class="nitesh-icon-box-container">
                                        <div class="nitesh-icon-box">
                                            <span class="pe-7s-cloud-upload nitesh-icon"></span>
                                        </div>
                                    </div>
                                    <div class="nitesh-list-info">
                                        <h4>Deployement</h4>
                                        <p>
                                            I deploye the project on the server, Great knowledge of server management.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="nitesh-icon-list-box">
                                    <div class="nitesh-icon-box-container">
                                        <div class="nitesh-icon-box">
                                            <span class="pe-7s-config nitesh-icon"></span>
                                        </div>
                                    </div>
                                    <div class="nitesh-list-info">
                                        <h4>Maintenance</h4>
                                        <p>
                                            Maintenance always the best part of project manangement, it really need in a project.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Me Clients -->
            <div class="my-clients-logo">
                <div class="nitesh-container">
                    <div class="mrnitesh-main-title">
                        <h2>
                            My Work
                            <span class="nitesh-line"></span>
                        </h2>
                    </div>
                    <div class="clients-slider">
                        <div class="client-logo">
                            <a href="#" target="_blank">
                                <img src="images/client/client_1.png" alt="">
                            </a>
                        </div>
                        <div class="client-logo">
                            <a href="#" target="_blank">
                                <img src="images/client/client_2.png" alt="">
                            </a>
                        </div>
                        <div class="client-logo">
                            <a href="#" target="_blank">
                                <img src="images/client/client_3.png" alt="">
                            </a>
                        </div>
                        <div class="client-logo">
                            <a href="#" target="_blank">
                                <img src="images/client/client_4.png" alt="">
                            </a>
                        </div>
                        <div class="client-logo">
                            <a href="#" target="_blank">
                                <img src="images/client/client_5.png" alt="">
                            </a>
                        </div>
                        <div class="client-logo">
                            <a href="#" target="_blank">
                                <img src="images/client/client_6.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Facts About Me -->
            <div class="nitesh-facts-about-me">
                <div class="nitesh-container overlay">
                    <div class="mrnitesh-main-title">
                        <h2>
                            Awesome Facts
                            <span class="nitesh-line"></span>
                        </h2>
                    </div>
                    <div class="nitesh-info-list">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6">
                                <div class="single-info">
                                    <p class="info-icon">
                                        <i class="pe-7s-display1"></i>
                                        <span class="facts-numbers" data-from="1" data-to="14" data-speed="1100"></span>
                                    </p>
                                    <h3> PROJECTS </h3>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="single-info">
                                    <p class="info-icon">
                                        <i class="pe-7s-alarm"></i>
                                        <span class="facts-numbers" data-from="70" data-to="5400" data-speed="1100"></span>
                                    </p>
                                    <h3> Work Hours </h3>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="single-info">
                                    <p class="info-icon">
                                        <i class="pe-7s-headphones"></i>
                                        <span class="facts-numbers" data-from="30" data-to="150" data-speed="1100"></span>
                                    </p>
                                    <h3> Customer SUPPORT </h3>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="single-info">
                                    <p class="info-icon">
                                        <i class="pe-7s-smile"></i>
                                        <span class="facts-numbers" data-from="1" data-to="11" data-speed="1100"></span>
                                    </p>
                                    <h3> Happy Clients </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Testimonials -->
            <!-- <div class="testimonials">
                <div class="nitesh-container">
                    <div class="mrnitesh-main-title">
                        <h2>
                            Testimonials
                            <span class="nitesh-line"></span>
                        </h2>
                    </div>
                    <div class="testimonials-slider-container"> -->
                        <!-- Testimonials Slider -->
                        <!-- <div class="testimonials-slider"> -->
                            <!-- Testimonials Slider Box -->
                            <!-- <div class="testi-box text-center">
                                <div class="img-box">
                                    <img src="images/11.png" alt="">
                                </div>
                                <div class="testi-user-info">
                                    <h3>Saad Tarek</h3>
                                    <h5>Designer at Upwork</h5>
                                </div>
                                <div class="testi-desc-box">
                                    <i>“</i>
                                    <p>
                                        Praesent et purus ac libero euismod egestas. Aliquam feugiat luctus eros eget
                                        maecenas
                                        semper pulvinar. tiam in auctor tortor. Aliquam vulputate risus placerat congue
                                        ornare.
                                        Donec semper odio non efficitur congue.
                                    </p>
                                    <i>”</i>
                                </div>
                            </div> -->
                            <!-- Testimonials Slider Box -->
                            <!-- <div class="testi-box text-center">
                                <div class="img-box">
                                    <img src="images/1.jpg" alt="">
                                </div>
                                <div class="testi-user-info">
                                    <h3>Amanda Catmull</h3>
                                    <h5>Designer at ProGlyphs</h5>
                                </div>
                                <div class="testi-desc-box">
                                    <i>“</i>
                                    <p>
                                        Praesent et purus ac libero euismod egestas. Aliquam feugiat luctus eros eget
                                        maecenas
                                        semper pulvinar. tiam in auctor tortor. Aliquam vulputate risus placerat congue
                                        ornare.
                                        Donec semper odio non efficitur congue.
                                    </p>
                                    <i>”</i>
                                </div>
                            </div> -->
                            <!-- Testimonials Slider Box -->
                            <!-- <div class="testi-box text-center">
                                <div class="img-box">
                                    <img src="images/2.jpg" alt="">
                                </div>
                                <div class="testi-user-info">
                                    <h3>Bill Fox</h3>
                                    <h5>Designer at Amanidax</h5>
                                </div>
                                <div class="testi-desc-box">
                                    <i>“</i>
                                    <p>
                                        Praesent et purus ac libero euismod egestas. Aliquam feugiat luctus eros eget
                                        maecenas
                                        semper pulvinar. tiam in auctor tortor. Aliquam vulputate risus placerat congue
                                        ornare.
                                        Donec semper odio non efficitur congue.
                                    </p>
                                    <i>”</i>
                                </div>
                            </div> -->
                            <!-- Testimonials Slider Box -->
                            <!-- <div class="testi-box text-center">
                                <div class="img-box">
                                    <img src="images/3.jpg" alt="">
                                </div>
                                <div class="testi-user-info">
                                    <h3>Joey Higgins</h3>
                                    <h5>CEO Higgins&Geox</h5>
                                </div>
                                <div class="testi-desc-box">
                                    <i>“</i>
                                    <p>
                                        Praesent et purus ac libero euismod egestas. Aliquam feugiat luctus eros eget
                                        maecenas
                                        semper pulvinar. tiam in auctor tortor. Aliquam vulputate risus placerat congue
                                        ornare.
                                        Donec semper odio non efficitur congue.
                                    </p>
                                    <i>”</i>
                                </div>
                            </div>
                        </div> -->
                        <!-- Silder Paging -->
                       <!--  <ul class="paging-container list-unstyled text-center list-inline">
                            <li class="testimonials-arrows" id="testimonial-prevArrow">
                                <div class="testimonials-client-page"></div>
                                <i class="pe-7s-angle-left"></i>
                            </li>
                            <li id="testimonial-paging"></li>
                            <li class="testimonials-arrows" id="testimonial-nextArrow">
                                <div class="testimonials-client-page"></div>
                                <i class="pe-7s-angle-right"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
    <!-- End About Me ==================-->

    <!-- Start portfolio
    ===================================-->
    <section class="full-page" id="portfolio">
        <div class="portfolio overlay-container">
            <div class="nitesh-intro-section">
                <div class="overlay">
                    <div class="section-intro">
                            <div class="text-center nitesh-icon-box-container">
                                <div class="nitesh-icon-box">
                                    <span class="fa fa-laptop nitesh-icon"></span>
                                </div>
                            </div>
                            <h1 class="text-center">
                                PORTFOLIO
                            </h1>
                        </div>
                </div>
            </div>
            <div class="port-box">
                <div class="nitesh-container">
                    <div class="mrnitesh-main-title">
                        <h2>
                            Knowledge
                            <span class="nitesh-line"></span>
                        </h2>
                    </div>
                    <div class="text-center">
                        <ul class="portfolio-sorting list-unstyled text-center nitesh-taps">
                            <li class="active" data-filter="*">All</li>
                            <li data-filter=".front-end">Front End</li>
                            <li data-filter=".back-end">Back End</li>
                            <li data-filter=".api">API</li>
                            <li data-filter=".extra">Extras</li>

                        </ul> <!--end portfolio sorting -->
                    </div>
                    <div class="portfolio-items">
                        <div class="row grid">
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item front-end">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-codepen"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">Angular</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-lin glyphicon glyphicon-linkglyphicon glyphicon-linkglyphicon glyphicon-linkglyphicon glyphicon-linkglyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Front End <i class="fa fa-codepen pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/angular.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item back-end">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-code"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">Laravel</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Back End <i class="fa fa-code pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/laravel.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item api">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-paint-brush"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">API</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>API <i class="fa fa-paint-brush pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/api.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item front-end">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-codepen"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">Bootstrap</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Front End <i class="fa fa-codepen pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/bootstrap.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item front-end">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-codepen"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">CSS3</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Front End <i class="fa fa-codepen pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/css3.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item front-end">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-codepen"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">HTML</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Front End <i class="fa fa-codepen pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/html.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item back-end">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-code"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">JavaScript</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Back End <i class="fa fa-code pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/javascript.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item back-end">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-code"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">PHP</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Back End <i class="fa fa-code pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/php.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-4 col-md-6 col-sm-6 grid-item back-end">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-code"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">Ajax</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Back End <i class="fa fa-code pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/ajax.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item extra">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-code"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">GIT</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Medium <i class="fa fa-code pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/git.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item extra">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-code"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">SQL</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Data Base <i class="fa fa-code pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/sql.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 grid-item extra">
                                <div class="portfolio-item">
                                    <div class="port-data">
                                        <div class="port-data-front-card"><i class="fa fa-code"></i></div>
                                        <div class="port-data-back-card">
                                            <h4 class="project-name">SSH</h4>
                                            <a href="#" target="_blank" class="port-show-info glyphicon glyphicon-link"></a>
                                            <div class="project-category">
                                                <h5>Push & Pull <i class="fa fa-code pull-right"></i></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="port-image">
                                        <img src="icon/ssh.jpg" width="100%" alt="" style="height:150px;">
                                    </div>
                                </div>
                            </div>
                            
                            

                        </div>
                    </div> <!--end row -->
                </div>
            </div>
        </div>
    </section>
    <!-- End portfolio ================-->

    <!-- Start Resume
    ===================================-->
    <section class="full-page" id="resume">
        <div class="resume overlay-container">
            <div class="nitesh-intro-section">
                <div class="overlay">
                    <div class="section-intro">
                            <div class="text-center nitesh-icon-box-container">
                                <div class="nitesh-icon-box">
                                    <span class="glyphicon glyphicon-leaf nitesh-icon"></span>
                                </div>
                            </div>
                            <h1 class="text-center">
                                RESUME
                            </h1>
                        </div>
                </div>
            </div>
            <div class="nitesh-container education-workHistory">
                <div class="mrnitesh-main-title">
                    <h2>
                        My Experience
                        <span class="nitesh-line"></span>
                    </h2>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="nitesh-education">
                            <h3 class="timeline-title">
                                <i class="pe-7s-study"></i> <span>Education</span>
                            </h3>
                            <div class="timeline-content">
                                <div class="timeline-box">
                                    <div class="timeline-date">
                                        2014 - 2017
                                    </div>
                                    <div class="timeline-info">
                                        <div class="timeline-info-header">
                                            <h3>YMCA University of Science & Technology</h3>
                                            <p>B.Tech, Information Technology</p>
                                        </div>
                                        <div class="timeline-info-details">
                                            <p style="text-align:justify;">
                                                YMCA University of Science and Technology (YMCA UST) is a state university located in Faridabad, in the state of Haryana, India. The YMCA University of Science and Technology, erstwhile YMCA Institute of Engineering, was established in the year 1969, as a joint venture of the National Council of YMCAs of India, Govt of Haryana, and the Central Agencies for Development Aid, Bonn, Germany.

National Assessment and Accreditation Council, an autonomous body funded by the UGC, has accredited the University with `A Grade`.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-box">
                                    <div class="timeline-date">
                                        2011 - 2014
                                    </div>
                                    <div class="timeline-info">
                                        <div class="timeline-info-header">
                                            <h3>C.R.Polytechnic,Rohtak(Haryana)</h3>
                                            <p>Diploma, Computer Science Engineering</p>
                                        </div>
                                        <div class="timeline-info-details">
                                            <p style="text-align:justify;">
                                                Lots of chances to grow myself were provided there, I always tried to hide me inside my eyes.
Few friends was there with but all were beautiful at this place. Thanks to this college.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="nitesh-experience">
                            <h3 class="timeline-title">
                                <i class="pe-7s-ribbon"></i> <span>Work History</span>
                            </h3>
                            <div class="timeline-content">
                                <div class="timeline-box">
                                    <div class="timeline-date">
                                        2017 - Present
                                    </div>
                                    <div class="timeline-info">
                                        <div class="timeline-info-header">
                                            <h3>TeamVariance</h3>
                                            <p>Software Web Developer</p>
                                        </div>
                                        <div class="timeline-info-details">
                                            <p style="text-align:justify;">
                                                Bringing you what we learned from online reading, articles, tutorials, resources, blogs and implementations. Team Variance comprises of passionate Engineers.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-box">
                                    <div class="timeline-date">
                                        Jan - June (2017)
                                    </div>
                                    <div class="timeline-info">
                                        <div class="timeline-info-header">
                                            <h3>MyCorporation PVT LTD.</h3>
                                            <p>Internship Training</p>
                                        </div>
                                        <div class="timeline-info-details">
                                            <p style="text-align:justify;">
                                                MyCorporation makes it easy and affordable to form an LLC, incorporate, file a trademark or copyright, and more.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="take-my-resume">
                    <a href="#" class="nitesh-btn-1"><span class="glyphicon glyphicon-cloud-download"></span> Take My Resume</a>
                </div>
            </div>
            <div class="nitesh-skills">
                <div class="overlay">
                    <div class="nitesh-container">
                        <div class="soft-skills">
                            <div class="mrnitesh-main-title">
                                <h2>
                                    Soft Skills
                                    <span class="nitesh-line"></span>
                                </h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="progress-container">
                                        <div class="progressName" id="pro1" data-value="0.8" data-color="#1894ff"></div>
                                        <h3>Communication</h3>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="progress-container">
                                        <div class="progressName" id="pro2" data-value="0.5" data-color="#1894ff"></div>
                                        <h3>Work In Team</h3>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="progress-container">
                                        <div class="progressName" id="pro4" data-value="0.6" data-color="#1894ff"></div>
                                        <h3>Speed</h3>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="progress-container">
                                        <div class="progressName" id="pro3" data-value="0.7" data-color="#1894ff"></div>
                                        <h3>creativity</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="technical-skills">
                            <div class="mrnitesh-main-title">
                                <h2>
                                    Technical Skills
                                    <span class="nitesh-line"></span>
                                </h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="progress-bar-container primary-style">
                                <span class="timer" data-from="0" data-to="95" data-speed="1100"
                                      data-refresh-interval="50">0</span>
                                        <div class="progress">
                                            <div class="progress-bar active" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                        </div>
                                        <h3>Laravel</h3>
                                    </div>
                                    <div class="progress-bar-container primary-style">
                                <span class="timer" data-from="0" data-to="70" data-speed="1100"
                                      data-refresh-interval="50">0</span>
                                        <div class="progress">
                                            <div class="progress-bar active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                        </div>
                                        <h3>Ajax</h3>
                                    </div>
                                </div> <!-- end the col -->
                                <div class="col-sm-6">
                                    <div class="progress-bar-container primary-style">
                                <span class="timer" data-from="0" data-to="90" data-speed="1100"
                                      data-refresh-interval="50">0</span>
                                        <div class="progress">
                                            <div class="progress-bar active" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                        </div>
                                        <h3>Javascript</h3>
                                    </div>
                                    <div class="progress-bar-container primary-style">
                                <span class="timer" data-from="0" data-to="80" data-speed="1100"
                                      data-refresh-interval="50">0</span>
                                        <div class="progress">
                                            <div class="progress-bar active" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                        </div>
                                        <h3>MySQL</h3>
                                    </div>
                                </div> <!-- end the col -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Skills ===================-->

    <!-- Start portfolio
    ===================================-->
    <section class="full-page" id="blog">
        <div class="blog overlay-container">
            <div class="nitesh-intro-section">
                <div class="overlay">
                    <div class="section-intro">
                        <div class="text-center nitesh-icon-box-container">
                            <div class="nitesh-icon-box">
                                <span class="fa fa-newspaper-o nitesh-icon"></span>
                            </div>
                        </div>
                        <h1 class="text-center">
                            BLOG
                        </h1>
                    </div>
                </div>
            </div>
            <div class="port-box">
                <div class="nitesh-container">
                    <div class="mrnitesh-main-title">
                        <h2>
                            Last Posts
                            <span class="nitesh-line"></span>
                        </h2>
                    </div>
                    <div class="posts">
                        <div class="row posts-grid">
                            <div class="col-lg-6 col-md-12 col-sm-6 post-grid-item">
                                <div>
                                    <article class="normal-post">
                                        <figure class="entry-header">
                                            <div class="post-image">
                                                <img src="images/blog/01.jpg" class="img-responsive" alt="">
                                            </div>
                                            <ul class="post-category list-unstyled">
                                                <li><a href="#">College</a></li>
                                                <li><a href="#">Room</a></li>
                                                <li><a href="#">Night Story</a></li>
                                            </ul>
                                            <ul class="post-share list-unstyled">
                                                <li class="open-post-share">
                                                    <span><i class="fa fa-share-alt"></i></span>
                                                    <ul class="post-share-links list-unstyled">
                                                        <li>
                                                            <a href="#" class="facebook-bg-hover">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="twitter-bg-hover">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="google-plus-bg-hover">
                                                                <i class="fa fa-google-plus"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </figure>
                                        <div class="entry-content">
                                            <div class="entry-post-info">
                                                <h4><a href="#">My Chaos Found Its Calm</a></h4>
                                                <p>Posted 28 Apr</p>
                                            </div>
                                            <div class="entry-expert">
                                                <p>
                                                    I kept looking for a place where I could find my peace but somehow could never do. When I was in first year of my Engineering College, I was staying in a P.G., with a lot of other students, but didn’t interact with any. I just saw them having lunch or coming and going to the college.
                                                </p>
                                                <div class="post-readMore">
                                                    <a href="https://medium.com/@Niteshsaini500/my-chaos-found-its-calm-861e1b25df33?source=user_profile---------2------------------" target="_blank" class="pull-left read-more-link">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                    <span class="pull-right"><a href="#">1 COMMENT</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="entry-footer">
                                            <ul class="entry-tags list-unstyled list-inline">
                                                <li><a href="#">Research</a></li>
                                                <li><a href="#">Development</a></li>
                                                <li><a href="#">Implementation</a></li>
                                            </ul>
                                        </div>
                                    </article>
                                </div>
                            </div>
                          <!--   <div class="col-lg-6 col-md-12 col-sm-6 post-grid-item">
                                <div>
                                    <article class="normal-post">
                                        <figure class="entry-header">
                                            <ul class="post-share list-unstyled">
                                                <li class="open-post-share">
                                                    <span><i class="fa fa-share-alt"></i></span>
                                                    <ul class="post-share-links list-unstyled">
                                                        <li>
                                                            <a href="#" class="facebook-bg-hover">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="twitter-bg-hover">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="google-plus-bg-hover">
                                                                <i class="fa fa-google-plus"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </figure>
                                        <div class="entry-content">
                                            <div class="entry-post-info">
                                                <h4><a href="#">Normal Post Style</a></h4>
                                                <p>
                                                    Posted 3 Nov
                                                </p>
                                            </div>
                                            <div class="entry-expert">
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consectetur distinctio dolorum ea facilis non repellendus velit voluptas voluptates? Accusantium at doloribus eum iste iusto, nam rem repudiandae voluptates. Inventore.
                                                </p>
                                                <div class="post-readMore">
                                                    <a href="#" class="pull-left read-more-link">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                    <span class="pull-right"><a href="#">1 COMMENT</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="entry-footer">
                                            <ul class="entry-tags list-unstyled list-inline">
                                                <li><a href="#">CSS</a></li>
                                                <li><a href="#">HTML</a></li>
                                                <li><a href="#">WORDPRESS</a></li>
                                            </ul>
                                        </div>
                                    </article>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-6 post-grid-item">
                                <div>
                                    <article class="normal-post">
                                        <figure class="entry-header">
                                            <div class="post-image">
                                                <img src="images/blog/02.jpg" class="img-responsive" alt="">
                                            </div>
                                            <ul class="post-category list-unstyled">
                                                <li><a href="#">creative</a></li>
                                                <li><a href="#">Front end</a></li>
                                                <li><a href="#">web design</a></li>
                                            </ul>
                                            <ul class="post-share list-unstyled">
                                                <li class="open-post-share">
                                                    <span><i class="fa fa-share-alt"></i></span>
                                                    <ul class="post-share-links list-unstyled">
                                                        <li>
                                                            <a href="#" class="facebook-bg-hover">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="twitter-bg-hover">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="google-plus-bg-hover">
                                                                <i class="fa fa-google-plus"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </figure>
                                        <div class="entry-content">
                                            <div class="entry-post-info">
                                                <h4><a href="#">Normal Post Style</a></h4>
                                                <p>Posted 12 Oct</p>
                                            </div>
                                            <div class="entry-expert">
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consectetur distinctio dolorum ea facilis non repellendus velit voluptas voluptates? Accusantium at doloribus eum iste iusto, nam rem repudiandae voluptates. Inventore.
                                                </p>
                                                <div class="post-readMore">
                                                    <a href="#" class="pull-left read-more-link">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                    <span class="pull-right"><a href="#">1 COMMENT</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="entry-footer">
                                            <ul class="entry-tags list-unstyled list-inline">
                                                <li><a href="#">CSS</a></li>
                                                <li><a href="#">HTML</a></li>
                                                <li><a href="#">WORDPRESS</a></li>
                                            </ul>
                                        </div>
                                    </article>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-6 post-grid-item">
                                <div>
                                    <article class="normal-post">
                                        <figure class="entry-header">
                                            <ul class="post-share list-unstyled">
                                                <li class="open-post-share">
                                                    <span><i class="fa fa-share-alt"></i></span>
                                                    <ul class="post-share-links list-unstyled">
                                                        <li>
                                                            <a href="#" class="facebook-bg-hover">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="twitter-bg-hover">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="google-plus-bg-hover">
                                                                <i class="fa fa-google-plus"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </figure>
                                        <div class="entry-content">
                                            <div class="entry-post-info">
                                                <h4><a href="#">Normal Post Style</a></h4>
                                                <p>
                                                    Posted 18 Aug
                                                </p>
                                            </div>
                                            <div class="entry-expert">
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consectetur distinctio dolorum ea facilis non repellendus velit voluptas voluptates? Accusantium at doloribus eum iste iusto, nam rem repudiandae voluptates. Inventore.
                                                </p>
                                                <div class="post-readMore">
                                                    <a href="#" class="pull-left read-more-link">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                    <span class="pull-right"><a href="#">1 COMMENT</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="entry-footer">
                                            <ul class="entry-tags list-unstyled list-inline">
                                                <li><a href="#">CSS</a></li>
                                                <li><a href="#">HTML</a></li>
                                                <li><a href="#">WORDPRESS</a></li>
                                            </ul>
                                        </div>
                                    </article>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-6 post-grid-item">
                                <div>
                                    <article class="normal-post">
                                        <figure class="entry-header">
                                            <div class="post-image">
                                                <img src="images/blog/03.jpg" class="img-responsive" alt="">
                                            </div>
                                            <ul class="post-category list-unstyled">
                                                <li><a href="#">creative</a></li>
                                                <li><a href="#">Front end</a></li>
                                                <li><a href="#">web design</a></li>
                                            </ul>
                                            <ul class="post-share list-unstyled">
                                                <li class="open-post-share">
                                                    <span><i class="fa fa-share-alt"></i></span>
                                                    <ul class="post-share-links list-unstyled">
                                                        <li>
                                                            <a href="#" class="facebook-bg-hover">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="twitter-bg-hover">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="google-plus-bg-hover">
                                                                <i class="fa fa-google-plus"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </figure>
                                        <div class="entry-content">
                                            <div class="entry-post-info">
                                                <h4><a href="#">Normal Post Style</a></h4>
                                                <p>Posted 2 Jul</p>
                                            </div>
                                            <div class="entry-expert">
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consectetur distinctio dolorum ea facilis non repellendus velit voluptas voluptates? Accusantium at doloribus eum iste iusto, nam rem repudiandae voluptates. Inventore.
                                                </p>
                                                <div class="post-readMore">
                                                    <a href="#" class="pull-left read-more-link">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                    <span class="pull-right"><a href="#">1 COMMENT</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="entry-footer">
                                            <ul class="entry-tags list-unstyled list-inline">
                                                <li><a href="#">Design</a></li>
                                                <li><a href="#">HTML</a></li>
                                                <li><a href="#">WEB</a></li>
                                            </ul>
                                        </div>
                                    </article>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-6 post-grid-item">
                                <div>
                                    <article class="normal-post">
                                        <figure class="entry-header">
                                            <div class="post-image">
                                                <img src="images/blog/04.jpg" class="img-responsive" alt="">
                                            </div>
                                            <ul class="post-category list-unstyled">
                                                <li><a href="#">creative</a></li>
                                                <li><a href="#">Front end</a></li>
                                                <li><a href="#">web design</a></li>
                                            </ul>
                                            <ul class="post-share list-unstyled">
                                                <li class="open-post-share">
                                                    <span><i class="fa fa-share-alt"></i></span>
                                                    <ul class="post-share-links list-unstyled">
                                                        <li>
                                                            <a href="#" class="facebook-bg-hover">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="twitter-bg-hover">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="google-plus-bg-hover">
                                                                <i class="fa fa-google-plus"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </figure>
                                        <div class="entry-content">
                                            <div class="entry-post-info">
                                                <h4><a href="#">Normal Post Style</a></h4>
                                                <p>Posted 8 Mar</p>
                                            </div>
                                            <div class="entry-expert">
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consectetur distinctio dolorum ea facilis non repellendus velit voluptas voluptates? Accusantium at doloribus eum iste iusto, nam rem repudiandae voluptates. Inventore.
                                                </p>
                                                <div class="post-readMore">
                                                    <a href="#" class="pull-left read-more-link">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                    <span class="pull-right"><a href="#">1 COMMENT</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="entry-footer">
                                            <ul class="entry-tags list-unstyled list-inline">
                                                <li><a href="#">CSS</a></li>
                                                <li><a href="#">HTML</a></li>
                                                <li><a href="#">WORDPRESS</a></li>
                                            </ul>
                                        </div>
                                    </article>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End portfolio ================-->

    <!-- Start Contact
    ===================================-->
    <section class="full-page" id="contact">
        <div class="contact overlay-container">
            <div class="nitesh-intro-section">
                <div class="overlay">
                    <div class="section-intro">
                            <div class="text-center nitesh-icon-box-container">
                                <div class="nitesh-icon-box">
                                    <span class="glyphicon glyphicon-headphones nitesh-icon"></span>
                                </div>
                            </div>
                            <h1 class="text-center">
                                CONTACT ME
                            </h1>
                        </div>
                </div>
            </div>
            <div class="contact-ways">
                <div class="nitesh-container">
                    <div class="mrnitesh-main-title">
                        <h2>
                            Get In Touch
                            <span class="nitesh-line"></span>
                        </h2>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="nitesh-icon-list-box">
                                <div class="nitesh-icon-box-container">
                                    <div class="nitesh-icon-box">
                                        <span class="glyphicon glyphicon-map-marker nitesh-icon"></span>
                                    </div>
                                </div>
                                <div class="nitesh-list-info">
                                    <h4>Location</h4>
                                    <p>
                                        Malviya Nagar, New Delhi
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="nitesh-icon-list-box">
                                <div class="nitesh-icon-box-container">
                                    <div class="nitesh-icon-box">
                                        <span class="glyphicon glyphicon-phone nitesh-icon"></span>
                                    </div>
                                </div>
                                <div class="nitesh-list-info">
                                    <h4>Phone</h4>
                                    <p>
                                        +91-8587-9697-38
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="nitesh-icon-list-box">
                                <div class="nitesh-icon-box-container">
                                    <div class="nitesh-icon-box">
                                        <span class="fa fa-envelope nitesh-icon"></span>
                                    </div>
                                </div>
                                <div class="nitesh-list-info">
                                    <h4>Email</h4>
                                    <p>
                                        Nitesh@niteshsaini.com
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-form">
                <div class="overlay">
                    <div class="nitesh-container">
                        <div class="mrnitesh-main-title">
                            <h2>
                                Send Message
                                <span class="nitesh-line"></span>
                            </h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                                <form class="nitesh-form" action="#" method="post" id="contact_form">
                                    <div class="input-row">
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Message About .." name="messageAbout" data-messageAboutValidation>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Your Full Name" name="full_name" data-nameValidation>
                                        </div>
                                    </div>
                                    <div class="input-row">
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="E-mail" name="email" data-emailValidation>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="phone" name="phone" data-phoneValidation>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Your Message .." name="message" data-messageValidation></textarea>
                                    </div>
                                    <div>
                                        <button class="btn nitesh-btn-1 btn-block btn-rgba" type="submit" name="submit_contact_form">
                                            <i class="fa fa-send-o"></i> Send Your Message
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact ==================-->

</main>
<?php include ('footer.php'); ?>